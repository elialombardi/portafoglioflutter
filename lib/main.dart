import 'dart:async';
import 'package:flutter/rendering.dart';
import 'package:flutter/material.dart';
import 'package:portafoglioflutter/api/api.dart';
import 'package:portafoglioflutter/pages/accounts_page.dart';
import 'package:portafoglioflutter/pages/login.dart';
import 'package:portafoglioflutter/pages/registration.dart';
import 'package:portafoglioflutter/providers/bloc_provider.dart';
import 'package:portafoglioflutter/utils/network_util.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

void main() {
  debugPaintSizeEnabled = true;
  runApp(BlocProvider(
    api: Api(),
    appName: 'Portafoglio',
    flavorName: 'development',
    // apiBaseUrl: 'http://172.21.212.129:5000/api',
    apiBaseUrl: 'http://172.28.127.209:5000/api',
    child: new MyApp(),
  ));
}

class MyApp extends StatelessWidget {
  Future setup(BuildContext context, Widget _defaultHome) async {
    final blocProvider = BlocProvider.of(context);
    NetworkUtil().baseApiUrl = blocProvider.apiBaseUrl;

    final prefs = await SharedPreferences.getInstance();

    String email = prefs.getString('email');

    if (email != null && email.isNotEmpty) {
      try {
        await blocProvider.authBloc
            .loginUser(email, prefs.getString('password'));
        // Navigator.pushReplacementNamed(context, '/accounts');
      } catch (ex) {
        print(ex);
      }
    }
  }

  MaterialApp loadApp(BlocProvider config, Widget _defaultHome) {
    return MaterialApp(
      title: config.appName,
      theme: ThemeData.light(),
      home: NetworkUtil().isUserAuthenticated() ? AccountsPage() : LoginPage(),
      // initialRoute: config.,
      routes: {
        // When we navigate to the "/" route, build the FirstScreen Widget
        '/login': (context) => LoginPage(),
        // When we navigate to the "/second" route, build the SecondScreen Widget
        '/register': (context) => RegistrationPage(),
        '/accounts': (context) => AccountsPage(),
      },
      localizationsDelegates: [
        // ... app-specific localization delegate[s] here
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
      ],
      supportedLocales: [
        const Locale('it', 'IT'),
        const Locale('en', 'US'), // English
        // ... other locales the app supports
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    final blocProvider = BlocProvider.of(context);

    Widget _defaultHome;

    return FutureBuilder(
        future: setup(context, _defaultHome),
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          switch (snapshot.connectionState) {
            // case ConnectionState.none: return new Text('Press button to start');
            case ConnectionState.waiting:
              return Center(child: CircularProgressIndicator());
              break;
            default:
              if (snapshot.hasError)
                return new Text('Error: ${snapshot.error}');
              else
                return loadApp(blocProvider, _defaultHome);
          }
        });
  }
}
