import 'dart:async';

import 'package:portafoglioflutter/models/user.dart';
import 'package:portafoglioflutter/utils/network_util.dart';
import 'dart:io' as io;

class UsersApi {

  Future<bool> registerUser(email, password) {
      return NetworkUtil().post(
        url: '/users', 
        headers: {io.HttpHeaders.CONTENT_TYPE: 'application/json'},
        body: User(email: email, password: password))
        .then((_) { return true; })
        .catchError((_) { return false; }); 
    }
    

  Future<List<User>> fetchUsers(String baseUrl) async {
    List<User> list = [];

    await NetworkUtil()
      .get(url: '/users', authenticatedRequest: true)
      .then((body) => body['data'] as List)
      .then((users) => 
        users.forEach((user) => list.add(User.fromJson(user))))
      .catchError((_) => throw Exception('Failed to load users'));

    return list;
  }
}
