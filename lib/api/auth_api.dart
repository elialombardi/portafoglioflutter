import 'dart:async';
import 'dart:io' as io;
import 'package:portafoglioflutter/models/user.dart';
import 'package:portafoglioflutter/utils/network_util.dart';

class AuthApi {
    Future loginUser(String email, String password) async {
    return NetworkUtil().post(
        url: '/auth/login',
        headers: {io.HttpHeaders.CONTENT_TYPE: 'application/json'},
        body: User(email: email, password: password));
  }
}