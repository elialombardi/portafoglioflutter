import 'dart:async';
import 'dart:io' as io;

import 'package:portafoglioflutter/models/payment.dart';
import 'package:portafoglioflutter/utils/network_util.dart';

class PaymentsApi {
  Future<List<Payment>> getPayments(
      {Map<String, String> queryParams = const {}}) async {
    final data = await NetworkUtil()
        .get(url: '/payments', authenticatedRequest: true, query: queryParams);

    if (data != null) {
      return (data['data'] as List)
          .map((e) => new Payment.fromJson(e))
          .toList();
    } else {
      // If that response was not OK, throw an error.
      throw Exception('Failed to load payments');
    }
  }

  Future<Payment> getPayment(String paymentId) async {
    print('Fetching Payment $paymentId');

    return NetworkUtil()
        .get(url: '/payments/$paymentId', authenticatedRequest: true)
        .then((body) => Payment.fromJson(body['data']))
        .catchError((_) => throw Exception('Failed to load Payment'));
  }

  Future<Payment> savePayment(Payment payment) async {
    final res = await NetworkUtil().post(
        url: '/payments',
        body: payment,
        headers: {io.HttpHeaders.CONTENT_TYPE: 'application/json'},
        authenticatedRequest: true);

    if (res != null && res['data'] != null) {
      return Payment.fromJson(res['data']);
    } else {
      // If that response was not OK, throw an error.
      throw Exception('Failed to load Payments');
    }
  }

  Future<void> deletePayment(String paymentId) async {
    final res = await NetworkUtil()
        .delete(url: '/payments/$paymentId', authenticatedRequest: true);

    if (res == null || res['isSuccess'] == false) {
      throw Exception('Failed to load Payments');
    }
  }
}
