import 'package:portafoglioflutter/api/accounts_api.dart';
import 'package:portafoglioflutter/api/auth_api.dart';
import 'package:portafoglioflutter/api/payments_api.dart';
import 'package:portafoglioflutter/api/users_api.dart';

class Api extends AccountsApi with AuthApi, UsersApi, PaymentsApi {
  
}