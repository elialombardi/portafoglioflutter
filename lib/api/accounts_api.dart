import 'dart:async';
import 'dart:io' as io;
import 'package:portafoglioflutter/models/account.dart';
import 'package:portafoglioflutter/utils/network_util.dart';

class AccountsApi {
  Future<List<Account>> getAccounts() async {
    List<Account> list = [];
    print('Fetching accounts');

    await NetworkUtil()
        .get(url: '/accounts', authenticatedRequest: true)
        .then((body) => body['data'] as List)
        .then((accounts) =>
            accounts.forEach((account) => list.add(Account.fromJson(account))))
        .catchError((_) => throw Exception('Failed to load accounts'));

    return list;
  }

  Future<Account> getAccount(String accountId) async {
    print('Fetching account $accountId');

    return NetworkUtil()
        .get(url: '/accounts/$accountId', authenticatedRequest: true)
        .then((body) => Account.fromJson(body['data']))
        .catchError((_) => throw Exception('Failed to load account'));
  }

  Future<Account> saveAccount(Account account) async {
    final res = await NetworkUtil().post(
        url: '/accounts',
        body: account,
        headers: {io.HttpHeaders.CONTENT_TYPE: 'application/json'},
        authenticatedRequest: true);

    if (res != null && res['data'] != null) {
      return Account.fromJson(res['data']);
    } else {
      // If that response was not OK, throw an error.
      throw Exception('Failed to load accounts');
    }
  }

  Future<void> deleteAccount(String accountId) async {
    final res = await NetworkUtil()
        .delete(url: '/accounts/$accountId', authenticatedRequest: true);

    if (res == null || res['isSuccess'] == false) {
      throw Exception('Failed to load accounts');
    }
  }
}
