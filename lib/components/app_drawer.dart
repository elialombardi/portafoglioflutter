import 'package:flutter/material.dart';
import 'package:portafoglioflutter/components/accounts_list_item.dart';
import 'package:portafoglioflutter/models/account.dart';
import 'package:portafoglioflutter/providers/bloc_provider.dart';

class AppDrawer extends StatelessWidget {
  Widget _buildListView(List<Account> accounts) {
    return Column(
      children: accounts.map((account) => AccountsListItem(account)).toList(),
    );
  }


  Widget _buildAccounts(BlocProvider blocProvider) {
    // final blocProvider = BlocProvider.of(context);
    
    return StreamBuilder<List<Account>>(
        stream: blocProvider.accountsBloc.outAccountsList,
        builder: (BuildContext context, AsyncSnapshot<List<Account>> snapshot) {
          if (snapshot.hasData) {
            // return _buildListView();
            return _buildListView(snapshot.data);
          } else if (snapshot.hasError) {
            return Text("${snapshot.error}");
          }
          blocProvider.accountsBloc.updateAccounts();

          // By default, show a loading spinner
          return Center(child: CircularProgressIndicator());
        });
  }

  Widget _buildAccountsSectionHeader(BuildContext context) {
    return ListTile(
      title: Text('Accounts'),
      leading: Icon(Icons.folder),
      onTap: () {
        // Update the state of the app
        // ...
        // Then close the drawer
        // Navigator.pop(context);
        Navigator.pushReplacementNamed(context, '/accounts');
      },
    );
  }
  

  Widget _buildCarSection(BuildContext context) {
    return ListTile(
      leading: Icon(Icons.directions_car),
      title: Text('Car'),
      trailing: new Chip(
        backgroundColor: Colors.redAccent,
        label: new Text('Coming soon', style: TextStyle(color: Colors.white),),
      ),
      onTap: () {
        Navigator.pop(context);
      },
    );
  }

  Widget _buildLogoutSection(BuildContext context) {
    return ListTile(
      leading: Icon(Icons.exit_to_app),
      title: Text('Logout'),
      onTap: () {
        BlocProvider.of(context).authBloc.logoutUser();
        Navigator.pop(context);
        Navigator.pushReplacementNamed(context, '/login');
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    final BlocProvider blocProvider = BlocProvider.of(context);
    return Drawer(
        child: ListView(
          children: <Widget>[
            _buildAccountsSectionHeader(context),
            _buildAccounts(blocProvider),
            _buildCarSection(context),
            _buildLogoutSection(context),
          ],
        ),
      );
  }
}