import 'package:flutter/material.dart';
import 'package:portafoglioflutter/components/app_drawer.dart';
import 'package:portafoglioflutter/components/payments_list_view.dart';
import 'package:portafoglioflutter/models/account.dart';

class DetailScreen extends StatelessWidget {
  // Declare a field that holds the Todo
  final Account account;

  // In the constructor, require a Todo
  DetailScreen({Key key, @required this.account}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // Use the Todo to create our UI
    return Scaffold(
      appBar: AppBar(
        title: Text("${account.title}"),
      ),
      drawer: new AppDrawer(),
      body: Column(
        // padding: EdgeInsets.all(16.0),
        children: [
          Text('${account.description}'),
          new PaymentsListView(account.payments)
        ]
      )
    );
  }
}
