import 'package:flutter/material.dart';
import 'package:portafoglioflutter/models/payment.dart';
import 'package:portafoglioflutter/providers/bloc_provider.dart';

class PaymentForm extends StatefulWidget {
  @override
  _PaymentFormState createState() => _PaymentFormState();
}

class _PaymentFormState extends State<PaymentForm> {
  final _formKey = GlobalKey<FormState>();
  final Map<String, dynamic> _formData = {
    'title': null,
    'description': null,
    'isIncome': true,
    'amount': null,
    'date': null,
    'account': null,
    'tags': null,
  };
  BlocProvider blocProvider;

  Widget _buildAmountInput() {
    return TextFormField(
      decoration: InputDecoration(
        labelText: 'Amount',
      ),
      keyboardType: TextInputType.number,
      validator: (value) {
        if (value.isEmpty) {
          return 'Required';
        }
      },
      onSaved: (String value) {
        _formData['amount'] = value;
      },
    );
  }

  Widget _buildIsIncomeCheckbox() {
    return Row(
      children: <Widget>[
        new RadioListTile(
          title: const Text('Income'),
          value: true,
          selected: true,
          groupValue: _formData['isIncome'],
          onChanged: (value) {
            setState(() {
              _formData['isIncome'] = value;
            });
          },
        ),
        new RadioListTile(
          title: const Text('Outcome'),
          groupValue: _formData['isIncome'],
          value: false,
          onChanged: (value) {
            setState(() {
              _formData['isIncome'] = value;
            });
          },
        ),
      ],
    );
  }

  Widget _buildDateInput() {
    return TextFormField(
      decoration: InputDecoration(
        labelText: 'Date',
      ),
      keyboardType: TextInputType.datetime,
      validator: (value) {
        if (value.isEmpty) {
          return 'Required';
        }
      },
      onSaved: (String value) {
        _formData['amount'] = value;
      },
    );
  }

  Widget _buildTitleInput() {
    return TextFormField(
      decoration: InputDecoration(
        labelText: 'Title',
      ),
      validator: (value) {
        if (value.isEmpty) {
          return 'Please enter some text';
        }
      },
      onSaved: (String value) {
        _formData['title'] = value;
      },
    );
  }

  Widget _buildDescriptionInput() {
    return TextFormField(
      decoration: InputDecoration(
        labelText: 'Description',
      ),
      validator: (value) {
        if (value.isEmpty) {
          return 'Please enter some text';
        }
      },
      onSaved: (String value) {
        _formData['description'] = value;
      },
    );
  }

  Widget _buildSubmitButton(Size screenSize) {
    return Container(
      width: screenSize.width,
      child: new RaisedButton(
        child: new Text(
          'Save',
          style: new TextStyle(color: Colors.white),
        ),
        onPressed: _submit,
        color: Colors.blue,
      ),
      margin: new EdgeInsets.only(top: 20.0),
    );
  }

  void _submit() async {
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();

      await blocProvider.paymentsBloc.savePayment(Payment(
          title: _formData['title'],
          description: _formData['description'],
          isIncome: _formData['isIncome'],
          date: _formData['date'],
          account: _formData['account'],
          tags: _formData['tags'],
          amount: _formData['amount']));

      Navigator.pop(context);
    }
  }

  @override
  Widget build(BuildContext context) {
    final Size screenSize = MediaQuery.of(context).size;
    blocProvider = BlocProvider.of(context);
    print("Here");
    return new Container(
        alignment: Alignment.topCenter,
        child: SingleChildScrollView(
            child: Container(
                padding: EdgeInsets.all(20.0),
                child: Form(
                  key: _formKey,
                  child: Column(
                    children: <Widget>[
                      _buildAmountInput(),
                      // _buildIsIncomeCheckbox(),
                      _buildDateInput(),
                      _buildTitleInput(),
                      _buildDescriptionInput(),
                      _buildSubmitButton(screenSize)
                    ],
                  ),
                ))));
  }
}
