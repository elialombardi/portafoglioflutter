// Define a Custom Form Widget
import 'dart:async';

import 'package:flutter/material.dart';
import 'package:portafoglioflutter/providers/bloc_provider.dart';
import 'package:portafoglioflutter/models/user.dart';

class UserForm extends StatefulWidget {
  final bool isRegistration;
  final User user;
  UserForm({@required this.isRegistration, this.user}) : super();
  @override
  UserFormState createState() {
    return UserFormState();
  }
}

// Define a corresponding State class. This class will hold the data related to 
// the form.
class UserFormState extends State<UserForm> {
  // Create a global key that will uniquely identify the Form widget and allow
  // us to validate the form
  //
  // Note: This is a `GlobalKey<FormState>`, not a GlobalKey<MyCustomFormState>! 
  final _formKey = GlobalKey<FormState>();
  // final _passwordController = TextEditingController();
  final Map<String, dynamic> _formData = {
    'email': null,
    'password': null,
    'passwordConf': null,
  };

  BlocProvider blocProvider;

  Future registerAction(BuildContext context) async {
    String message = 'Registered ${_formData['email']}';
    if(_formData['password'] != _formData['passwordConf'])
      message = 'Passwords are different';
    else {
      
      await blocProvider
        .usersBloc
        .registerUser(_formData['email'], _formData['password'])
        .then((result) {
          if(!result)
            message = 'Error registering user';
        });
    } 

    Scaffold
      .of(context)
      .showSnackBar(SnackBar(content: Text(message)));
  }

  Future loginAction(context) async {
    try {
      await blocProvider.authBloc.loginUser(_formData['email'], _formData['password']);

      Navigator.pushReplacementNamed(context, '/accounts');
    } catch(e) {
      Scaffold
        .of(context)
        .showSnackBar(SnackBar(content: Text('Error logging in user')));
    }

  }
  void submit(BuildContext context) async {
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();

      if(widget.isRegistration)
        await registerAction(context);
      else
        await loginAction(context);
    }
  }

  Widget _buildEmailTextInput() {
    return TextFormField(
      decoration: InputDecoration(
        labelText: 'Email',
      ),
      keyboardType: TextInputType.emailAddress,
      initialValue: widget.user?.email ?? '',
      validator: (value) {
        if (value.isEmpty) {
          return 'Please enter some text';
        }
      },
      onSaved: (String value) {
        _formData['email'] = value;
      },
    );
  }

  Widget _buildPasswordTextInput() {
    return TextFormField(
      // controller: _passwordController,
      // The validator receives the text the user has typed in
      obscureText: true,
      decoration: InputDecoration(
        labelText: 'Password',
      ),
      initialValue: widget.user?.password ?? '',
      validator: (value) {
        if (value.isEmpty) {
          return 'Please enter some text';
        }
      },
      onSaved: (String value) {
        _formData['password'] = value;
      },
    );
  }

  Widget _buildConfirmPasswordTextInput() {
    return !widget.isRegistration ? Container() : TextFormField(
      // The validator receives the text the user has typed in
      obscureText: true,
      decoration: InputDecoration(
        labelText: 'Confirm password',
      ),
      initialValue: '',
      validator: (value) {
        if (value.isEmpty) {
          return 'Please enter some text';
        // } else if(_passwordController.text != value) {
        //   print('password: ${_passwordController.text}');
        //   return 'Password is different';
        }
      },
      onSaved: (String value) {
        _formData['passwordConf'] = value;
      },
    );
  }

  Widget _buildPrimaryActionButton(Size screenSize, BuildContext context) {
    return Container(
      width: screenSize.width,
      child: new RaisedButton(
        child: new Text(
          widget.isRegistration ? 'Register' : 'Login',
          style: new TextStyle(
            color: Colors.white
          ),
        ),
        onPressed: () => submit(context),
        color: Colors.blue,
      ),
      margin: new EdgeInsets.only(
        top: 20.0
      ),
    );
  }

  Widget _buildSecondaryActionButton(Size screenSize) {
    return Container(
      width: screenSize.width,
      child: new RaisedButton(
        child: new Text(
          !widget.isRegistration ? 'Register' : 'Login',
          style: new TextStyle(
            color: Colors.grey
          ),
        ),
        onPressed: () { 
          widget.isRegistration ? Navigator.pop(context) : Navigator.pushNamed(context, '/register');
        },
        color: Colors.white,
      ),
      margin: new EdgeInsets.only(
        top: 20.0
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final Size screenSize = MediaQuery.of(context).size;
    blocProvider = BlocProvider.of(context);

    // Build a Form widget using the _formKey we created above
    return new Container(
      alignment: Alignment.topCenter,
      child: SingleChildScrollView(
      padding: EdgeInsets.all(20.0),
      child: Form(
        key: _formKey,
        child: Column(
          children: <Widget>[
            _buildEmailTextInput(),
            _buildPasswordTextInput(),
            _buildConfirmPasswordTextInput(),
            _buildPrimaryActionButton(screenSize, context),
            _buildSecondaryActionButton(screenSize),
          ],
        ),
      )
      )
    );
  }
}
