import 'dart:async';

import 'package:flutter/material.dart';
import 'package:portafoglioflutter/models/account.dart';
import 'package:portafoglioflutter/pages/account_page.dart';
import 'package:portafoglioflutter/providers/bloc_provider.dart';

class AccountsListItem extends StatelessWidget {
  final Account account;

  AccountsListItem(this.account);

  Future _onTap(BuildContext context) async {
    Account fullAccount = await BlocProvider.of(context).accountsBloc.getAccount(account.id);

    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => AccountPage(account: fullAccount),
      ),
    );
  }

  void _onDismissed(direction, context) async {
    await BlocProvider.of(context).accountsBloc.deleteAccount(account.id);

    Scaffold.of(context).showSnackBar(SnackBar(
          content: Text('Account removed successfully!'),
        ));
  }

  Widget _buildListTile(BuildContext context) {
    return ListTile(
      title: Text(account.title),
      subtitle: Text(account.description ?? ''),
      // leading: const Icon(Icons.folder),
      trailing: new Chip(
        backgroundColor:
            account.total < 0 ? Colors.redAccent : Colors.greenAccent,
        label: Text('${account.total.toStringAsFixed(2)} €'),
      ),
      onTap: () => _onTap(context),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Dismissible(
        background: ListTile(
          trailing: Icon(Icons.remove),
        ),
        key: Key(account.id),
        onDismissed: (direction) => _onDismissed(direction, context),
        child: _buildListTile(context));
  }
}
