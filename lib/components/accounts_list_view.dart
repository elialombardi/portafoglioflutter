import 'package:flutter/material.dart';
import 'package:portafoglioflutter/components/accounts_list_item.dart';
import 'package:portafoglioflutter/models/account.dart';
import 'package:portafoglioflutter/providers/bloc_provider.dart';

// class AccountsListView extends StatefulWidget {
//   @override
//   _AccountsListViewState createState() => _AccountsListViewState();
// }

// class _AccountsListViewState extends State<AccountsListView> {
//   List<Account> _accounts;
//   BlocProvider blocProvider;

class AccountsListView extends StatelessWidget {
  Widget _buildListView(List<Account> accounts) {
    return ListView.builder(
      itemCount: accounts.length,
      itemBuilder: (_, index) {
        return AccountsListItem(accounts[index]);
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    final blocProvider = BlocProvider.of(context);
    blocProvider.accountsBloc.updateAccounts();
    
    return StreamBuilder<List<Account>>(
        stream: blocProvider.accountsBloc.outAccountsList,
        builder: (BuildContext context, AsyncSnapshot<List<Account>> snapshot) {
          if (snapshot.hasData) {
            // return _buildListView();
            return _buildListView(snapshot.data);
          } else if (snapshot.hasError) {
            return Text("${snapshot.error}");
          }
          print('loading');
          // By default, show a loading spinner
          return CircularProgressIndicator();
        });

    // return FutureBuilder<List<Account>>(
    //   future: blocProvider.accountsBloc.fetchAccounts(),
    //   builder: (context, snapshot) {
    //     if (snapshot.hasData) {
    //       _accounts = snapshot.data;
    //       return _buildListView();
    //     } else if (snapshot.hasError) {
    //       return Text("${snapshot.error}");
    //     }

    //     // By default, show a loading spinner
    //     return CircularProgressIndicator();
    //   },
    // );
  }
}
