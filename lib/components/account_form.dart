// Define a Custom Form Widget
import 'package:flutter/material.dart';
import 'package:portafoglioflutter/models/account.dart';
import 'package:portafoglioflutter/providers/bloc_provider.dart';

class AccountForm extends StatefulWidget {
  final Account account;
  AccountForm({this.account}) : super();
  @override
  AccountFormState createState() {
    return AccountFormState(account);
  }
}

class AccountFormState extends State<AccountForm> {
  final _formKey = GlobalKey<FormState>();
  final Map<String, dynamic> _formData = {
    'title': null,
    'description': null,
  };
  AccountFormState(this.account) : super();

  Account account;
  BlocProvider blocProvider;


  Widget _buildTitleInput() {
    return TextFormField(
      decoration: InputDecoration(
        labelText: 'Title',
      ),
      validator: (value) {
        if (value.isEmpty) {
          return 'Please enter some text';
        }
      },
      onSaved: (String value) {
        _formData['title'] = value;
      },
    );
  }

  Widget _buildDescriptionInput() {
    return TextFormField(
      decoration: InputDecoration(
        labelText: 'Description',
      ),
      validator: (value) {
        if (value.isEmpty) {
          return 'Please enter some text';
        }
      },
      onSaved: (String value) {
        _formData['description'] = value;
      },
    );
  }

  Widget _buildSubmitButton(Size screenSize) {
    return Container(
      width: screenSize.width,
      child: new RaisedButton(
        child: new Text(
          'Save',
          style: new TextStyle(
            color: Colors.white
          ),
        ),
        onPressed: _submit,
        color: Colors.blue,
      ),
      margin: new EdgeInsets.only(
        top: 20.0
      ),
    );
  }

  void _submit() async {
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();

        await blocProvider.accountsBloc.saveAccount(Account(title: _formData['title'], description: _formData['description']));
        
        Navigator.pushReplacementNamed(context, '/accounts');
    }
  }
  
  @override
  Widget build(BuildContext context) {
    final Size screenSize = MediaQuery.of(context).size;
    blocProvider = BlocProvider.of(context);

    // Build a Form widget using the _formKey we created above
    return new Container(
      alignment: Alignment.topCenter,
      child: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.all(20.0),
          child: Form(
            key: _formKey,
            child: Column(
              children: <Widget>[
                _buildTitleInput(),
                _buildDescriptionInput(),
                _buildSubmitButton(screenSize)
              ],
            ),
          )
        )
      )
    );
  }
}
