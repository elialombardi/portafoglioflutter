import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:portafoglioflutter/models/payment.dart';
import 'package:portafoglioflutter/providers/bloc_provider.dart';

class PaymentsListView extends StatelessWidget {
  final List<Payment> payments;

  PaymentsListView(this.payments);

  void _onDismissed(BuildContext context, Payment payment) async {
    await BlocProvider.of(context).paymentsBloc.deletePayment(payment.id);

    Scaffold.of(context).showSnackBar(SnackBar(
          content: Text('Payment removed successfully!'),
        ));
  }

  Widget _buildListItem(BuildContext context, Payment payment) {
    return Dismissible(
      key: Key(payment.id),
      onDismissed: (direction) => _onDismissed(context, payment),
      child: ListTile(
        title: Text(
            '${payment.title} - ${new DateFormat('dd/MM/y').format(payment.date)}'),
        subtitle: Text(payment.description),
        leading: Chip(
          label: Text('${payment.amount.toStringAsFixed(2)} €'),
          backgroundColor:
              payment.isIncome ? Colors.greenAccent : Colors.redAccent,
        ),
        onTap: () {
          // Navigator.pushReplacement(
          //   context,
          //   MaterialPageRoute(
          //     builder: (context) => AccountPage(account: payments[index]),
          //   ),
          // );
        },
      ),
    );
  }

  Widget _buildListView() {
    return ListView.builder(
      itemCount: payments.length,
      itemBuilder: (context, index) {
        return _buildListItem(context, payments[index]);
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: payments.length > 0
          ? _buildListView()
          : Text(
              'No payments.',
              textAlign: TextAlign.left,
            ),
    );
    // final BlocProvider blocProvider = BlocProvider.of(context);
    // FutureBuilder<List<Payment>>(
    //   future: blocProvider.paymentsBloc.getPayments(accountId),
    //   builder: (context, snapshot) {
    //     switch (snapshot.connectionState) {
    //       // case ConnectionState.none: return new Text('Press button to start');
    //       case ConnectionState.waiting:
    //         return Center(child: CircularProgressIndicator());
    //       default:
    //         if (snapshot.hasError) {
    //           return new Text('Error: ${snapshot.error}');
    //         } else {
    //           return Expanded(child: snapshot.data.length > 0
    //               ? _buildListView(snapshot.data)
    //               : Text('No payments.', textAlign: TextAlign.left,),);
    //         }
    //     }
    //   },
    // );
  }
}
