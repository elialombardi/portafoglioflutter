import 'package:flutter/material.dart';
import 'package:portafoglioflutter/shopping-list.dart';
import 'package:portafoglioflutter/shopping-list-item.dart';

void main() {
  runApp(MaterialApp(
    title: 'Shopping App',
    home: ShoppingList(
      products: <Product>[
        Product(name: 'Eggs'),
        Product(name: 'Flour'),
        Product(name: 'Chocolate chips'),
      ],
    ),
  ));
}