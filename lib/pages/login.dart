import 'package:flutter/material.dart';
import 'package:portafoglioflutter/components/user_form.dart';

class LoginPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    return Scaffold(
      appBar: AppBar(
        title: Text('Login'),
      ),
      body: Center(
        child: UserForm(isRegistration: false,),
      ),
    );
  }

}