import 'package:flutter/material.dart';
import 'package:portafoglioflutter/components/app_drawer.dart';
import 'package:portafoglioflutter/components/payment_form.dart';
import 'package:portafoglioflutter/components/payments_list_view.dart';
import 'package:portafoglioflutter/models/account.dart';

class PaymentPage extends StatelessWidget {
  final Payment payment;

  PaymentPage({Key key, @required this.payment}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Add Payment"),
      ),
      drawer: AppDrawer(),
      body: Center(child: PaymentForm(),)
    );
  }
}

class Payment {}
