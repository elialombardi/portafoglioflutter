import 'package:flutter/material.dart';
import 'package:portafoglioflutter/components/account_form.dart';
import 'package:portafoglioflutter/components/accounts_list_view.dart';
import 'package:portafoglioflutter/components/app_drawer.dart';
import 'package:portafoglioflutter/models/account.dart';
import 'package:portafoglioflutter/providers/bloc_provider.dart';

class AccountsPage extends StatelessWidget {

  void addAccount(BuildContext context) {
    showDialog(
      context: context,
      builder: (_) => SimpleDialog(
        title: const Text('Add Account'),
        children: <Widget>[AccountForm(account: new Account(),)],
      ),
    );
    // Scaffold
    //   .of(context)
    //   .showSnackBar(SnackBar(content: Text('Aggiungi un account')));
  }
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    final blocProvider = BlocProvider.of(context);

    return Scaffold(
      appBar: AppBar(
        title: Text('Accounts'),
        centerTitle: true,
        actions: <Widget>[
          new IconButton(
            icon: Icon(Icons.refresh), 
            onPressed: () => blocProvider.accountsBloc.updateAccounts(),
          )
        ],
      ),
      drawer: new AppDrawer(),
      body: Center(
        child: AccountsListView(),
      ),
      floatingActionButton: new FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () => addAccount(context),
      ),
    );
  }

}