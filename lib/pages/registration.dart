import 'package:flutter/material.dart';
import 'package:portafoglioflutter/components/user_form.dart';

class RegistrationPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    return Scaffold(
      appBar: AppBar(
        title: Text('Register'),
      ),
      body: Center(
        child: UserForm(isRegistration: true,),
      ),
    );
  }

}