import 'package:flutter/material.dart';
import 'package:portafoglioflutter/components/app_drawer.dart';
import 'package:portafoglioflutter/components/payments_list_view.dart';
import 'package:portafoglioflutter/models/account.dart';
import 'package:portafoglioflutter/pages/payment_page.dart';

class AccountPage extends StatelessWidget {
  final Account account;

  AccountPage({Key key, @required this.account}) : super(key: key);

  Widget _buildUsersSection() {
    return Column(
      children: <Widget>[
        Text('Users: ', textAlign: TextAlign.left),
        Row(
          children: account.owners
              .map((owner) => Chip(
                    label: Text(owner.email[0]),
                  ))
              .toList(),
        ),
      ],
    );
  }

  void addPayment(BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => PaymentPage(payment: null),
      ),
    );
  }

  Widget _buildAddPaymentButton(BuildContext context) {
    return FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () => addPayment(context),
      );
  }

  @override
  Widget build(BuildContext context) {
    // Use the Todo to create our UI
    return Scaffold(
      appBar: AppBar(
        title: Text("${account.title} - ${account.total.toStringAsFixed(2)} €"),
      ),
      drawer: AppDrawer(),
      body: Container(
        padding: EdgeInsets.fromLTRB(16.0, 16.0, 16.0, 0.0),
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(account.description ?? '', textAlign: TextAlign.left),
              SizedBox(
                height: 10.0,
              ),
              _buildUsersSection(),
              SizedBox(
                height: 10.0,
              ),
              PaymentsListView(
                account.payments,
              )
            ]),
      ),
      floatingActionButton: _buildAddPaymentButton(context),
    );
  }
}
