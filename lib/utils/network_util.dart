import 'dart:async';
import 'dart:convert';
import 'dart:io' as io;
import 'package:http/http.dart' as http;
import 'package:meta/meta.dart';

class NetworkUtil {
  // next three lines makes this class a Singleton
  // static NetworkUtil _instance = new NetworkUtil.internal();
  // NetworkUtil.internal();
  // factory NetworkUtil() => _instance;

  static final NetworkUtil _singleton = new NetworkUtil._internal();

  factory NetworkUtil() {
    return _singleton;
  }

  NetworkUtil._internal();

  final JsonDecoder _decoder = new JsonDecoder();
  final JsonEncoder _encoder = new JsonEncoder();
  String baseApiUrl;
  String _authToken;

  bool isUserAuthenticated() {
    return _authToken != null && _authToken.trim().isNotEmpty;
  }

  void clearToken() {
    _authToken = null;
  }

  void goToLoginPage() {}

  Future<dynamic> post(
      {@required String url,
      bool authenticatedRequest: false,
      headers,
      body,
      encoding}) async {
    if (!authenticatedRequest || isUserAuthenticated()) {
      if (authenticatedRequest) {
        headers[io.HttpHeaders.AUTHORIZATION] = "Bearer $_authToken";
      }
      print('url: $baseApiUrl');
      final response = await http.post(baseApiUrl + url,
          body: _encoder.convert(body), headers: headers, encoding: encoding);

      final String res = response.body;
      final int statusCode = response.statusCode;

      if (statusCode == 200) {
        var decodedRes = _decoder.convert(res);

        if (decodedRes['token'] != null)
          _authToken = decodedRes['token'];
        else if (response.headers['renew'] != null &&
            response.headers['renew'].isNotEmpty)
          _authToken = response.headers['renew'];

        return decodedRes;
      } else {
        if (statusCode == 401) goToLoginPage();

        throw new Exception('Error posting $url');
      }
    } else {
      goToLoginPage();
      throw new Exception('Error posting $url');
    }
  }

  Future<dynamic> get(
      {@required String url,
      bool authenticatedRequest: false,
      Map<String, dynamic> query: const {}}) async {
    if (!authenticatedRequest || isUserAuthenticated()) {
      final response = await http.get(baseApiUrl + url,
          headers: authenticatedRequest == true
              ? {io.HttpHeaders.AUTHORIZATION: "Bearer $_authToken"}
              : {});

      final res = _decoder.convert(response.body);
      final int statusCode = response.statusCode;

      if (response.headers['renew'] != null &&
          response.headers['renew'].isNotEmpty)
        _authToken = response.headers['renew'];

      if (statusCode == 200) {
        return res;
      } else {
        if (statusCode == 401) goToLoginPage();
        // else
        //   return null;
        throw new Exception('Error requesting $url');
      }
    } else {
      goToLoginPage();
      throw new Exception('Error requesting $url');
    }
  }

  Future<dynamic> delete(
      {@required String url, bool authenticatedRequest: false}) async {
    if (!authenticatedRequest || isUserAuthenticated()) {
      final response = await http.delete(baseApiUrl + url,
          headers: authenticatedRequest == true
              ? {io.HttpHeaders.AUTHORIZATION: "Bearer $_authToken"}
              : {});

      final res = _decoder.convert(response.body);
      final int statusCode = response.statusCode;

      if (response.headers['renew'] != null &&
          response.headers['renew'].isNotEmpty)
        _authToken = response.headers['renew'];

      if (statusCode == 200) {
        return res;
      } else {
        if (statusCode == 401) goToLoginPage();
        // else
        //   return null;
        throw new Exception('Error requesting $url');
      }
    } else {
      goToLoginPage();
      throw new Exception('Error requesting $url');
    }
  }
}
