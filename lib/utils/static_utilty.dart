import 'package:portafoglioflutter/models/payment.dart';
import 'package:portafoglioflutter/models/payment_tag.dart';
import 'package:portafoglioflutter/models/user.dart';

class StaticUtility {
  static List<Payment> parsePaymentsMap(List paymentsJson) {
    // paymentsJson[0] is String => check if it has only the ids
    if (paymentsJson == null ||
        paymentsJson.length == 0 ||
        paymentsJson[0] is String) return [];

    return paymentsJson
        .map((paymentMap) =>
            paymentMap is String ? null : Payment.fromJson(paymentMap))
        .toList();
  }

  static List<User> parseUsersMap(List ownersJson) {
    // ownersJson[0] is String => check if it has only the ids
    if (ownersJson == null || ownersJson.length == 0 || ownersJson[0] is String)
      return [];

    return ownersJson
        .map((ownerMap) => ownerMap is String ? null : User.fromJson(ownerMap))
        .toList();
  }

  static List<PaymentTag> parsePaymentTagsMap(List paymentTagsJson) {
    // paymentTagsJson[0] is String => check if it has only the ids
    if (paymentTagsJson == null || paymentTagsJson.length == 0 || paymentTagsJson[0] is String)
      return [];

    return paymentTagsJson
        .map((paymentTagMap) => paymentTagMap is String ? null : PaymentTag.fromJson(paymentTagMap))
        .toList();
  }
}
