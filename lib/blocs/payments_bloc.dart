import 'dart:async';
import 'dart:collection';

import 'package:portafoglioflutter/api/api.dart';
import 'package:portafoglioflutter/models/payment.dart';
import 'package:rxdart/rxdart.dart';

class PaymentsBloc {

  // PublishSubject<List<Payment>> _paymentsController =
  //     PublishSubject<List<Payment>>();
  // Sink<List<Payment>> get _inPaymentsList => _paymentsController.sink;
  // Stream<List<Payment>> get outPaymentsList => _paymentsController.stream;

  final Api api;

  PaymentsBloc(this.api);

  Future updatePayments() {
    return api.getPayments().then((payments) {
      print('updating payments');
      // _inPaymentsList.add(UnmodifiableListView<Payment>(payments));
    });
  }

  Future getPayment(String paymentId) {
    return api.getPayment(paymentId);
  }

  Future savePayment(Payment payment) {
    return api.savePayment(payment).then((_) => updatePayments());
  }

  Future deletePayment(String paymentId) {
    return api.deletePayment(paymentId).then((_) => updatePayments());
  }

  void dispose() {
    // _paymentsController.close();
    // _indexController.close();
  }
}
