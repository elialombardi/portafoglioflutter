import 'dart:async';
import 'dart:collection';

import 'package:portafoglioflutter/api/api.dart';
import 'package:portafoglioflutter/models/account.dart';
import 'package:rxdart/rxdart.dart';

class AccountsBloc {

  PublishSubject<List<Account>> _accountsController =
      PublishSubject<List<Account>>();
  Sink<List<Account>> get _inAccountsList => _accountsController.sink;
  Stream<List<Account>> get outAccountsList => _accountsController.stream;

  final Api api;

  AccountsBloc(this.api) {
    print('new AccountsBloc');
    // _indexController.stream
    //     // take some time before jumping into the request (there might be several ones in a row)
    //     .bufferTime(Duration(microseconds: 500))
    //     // and, do not update where this is no need
    //     .where((batch) => batch.isNotEmpty)
    //     .listen(_handleIndexes);
  }

  // void _handleIndexes(List<int> indexes) {
  //   updateAccounts();
  // }

  Future updateAccounts() {
    return api.getAccounts().then((accounts) {
      print('updating accounts');
      _inAccountsList.add(UnmodifiableListView<Account>(accounts));
    });
  }

  Future getAccount(String accountId) {
    return api.getAccount(accountId);
  }

  Future saveAccount(Account account) {
    return api.saveAccount(account).then((_) => updateAccounts());
  }

  Future deleteAccount(String accountId) {
    return api.deleteAccount(accountId).then((_) => updateAccounts());
  }

  void dispose() {
    _accountsController.close();
    _inAccountsList.close();
    // _indexController.close();
  }
}
