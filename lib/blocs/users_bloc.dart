
import 'dart:async';

import 'package:portafoglioflutter/api/api.dart';

class UsersBloc {

  final Api api;

  UsersBloc(this.api);
  
  // PublishSubject<List<User>> _usersController =
  //     PublishSubject<List<User>>();
  // Sink<List<User>> get _inUsersList => _usersController.sink;
  // Stream<List<User>> get outUsersList => _usersController.stream;

  Future<bool> registerUser(String email, String password) async {
    return api.registerUser(email, password);
  }

  // void dispose() {
  //   _usersController.close();
  // }
}