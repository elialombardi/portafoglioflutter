
import 'dart:async';

import 'package:portafoglioflutter/api/api.dart';
import 'package:portafoglioflutter/utils/network_util.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AuthBloc {

  Api api;

  AuthBloc(this.api);

  Future logoutUser() async {
    final prefs = await SharedPreferences.getInstance();
    prefs.clear();
    NetworkUtil().clearToken();
  }

  Future loginUser(String email, String password) async {
    await api.loginUser(email, password); 

    final prefs = await SharedPreferences.getInstance();

    // set value
    prefs.setString('email', email);
    prefs.setString('password', password);
  }
}