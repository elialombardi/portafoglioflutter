// var PaymentTagSchema = new Schema({
//   title: { type: String, required: true },
//   description: String,
//   isPublic: { type: Boolean, default: false },
//   owners: [{ type: Schema.Types.ObjectId, ref: 'User' }],
//   payments: [{ type: Schema.Types.ObjectId, ref: 'PaymentTag' }]
// })

import 'package:portafoglioflutter/models/payment.dart';
import 'package:portafoglioflutter/utils/static_utilty.dart';

class PaymentTag {
  final String id;
  final String title;
  final String description;
  final bool isPublic;
  final List<Payment> payments;

  PaymentTag(
      {this.id, this.title, this.description, this.isPublic, this.payments});

  PaymentTag.fromJson(Map<String, dynamic> json)
      : id = json['_id'],
        title = json['title'],
        description = json['description'],
        isPublic = json['isPublic'],
        payments = StaticUtility.parsePaymentsMap(json['description']);
}
