class User {
  final String id;
  final String email;
  final String password;

  User({this.id, this.email, this.password});

  Map<String, dynamic> toJson() {
    var jsonObject = {
      'email': email,
      'password': password,
    };

    if(id != null)
      jsonObject['_id'] = id;

    return jsonObject;
  }

  User.fromJson(Map json) :
      id = json['_id'],
      email = json['email'],
      password = json['password'] ?? '';
}
