import 'package:portafoglioflutter/models/payment_tag.dart';
import 'package:portafoglioflutter/utils/static_utilty.dart';

class Payment {
  final String id;
  final String title;
  final String description;
  final bool isIncome;
  final double amount;
  final DateTime date;
  final String account;
  final List<PaymentTag> tags;

  Payment({
    this.id,
    this.title,
    this.description,
    this.isIncome,
    this.amount,
    this.date,
    this.account,
    this.tags,
  });

  factory Payment.fromJson(Map<String, dynamic> json) {
    return Payment(
      id: json['_id'],
      title: json['title'],
      description: json['description'],
      amount: double.tryParse(json['amount']),
      isIncome: json['isIncome'],
      date: DateTime.parse(json['date']),
      account: json['account'],
      tags: StaticUtility.parsePaymentTagsMap(json['tags']),
    );
  }
}
