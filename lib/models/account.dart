import 'package:portafoglioflutter/models/payment.dart';
import 'package:portafoglioflutter/models/user.dart';

import '../utils/static_utilty.dart';

class Account {
  final String id;
  final String title;
  final String description;
  final double total;
  final List<User> owners;
  final List<Payment> payments;

  Account(
      {this.id,
      this.title,
      this.description,
      this.total,
      this.owners = const [],
      this.payments = const []});

  Map<String, dynamic> toJson() {
    var jsonObject = {
      'title': title,
      'description': description,
    };

    if (id != null) jsonObject['_id'] = id;

    return jsonObject;
  }

  factory Account.fromJson(Map<String, dynamic> json) {
    return Account(
      id: json['_id'],
      title: json['title'],
      description: json['description'],
      total: double.tryParse(json['total']),
      owners: StaticUtility.parseUsersMap(json['owners']),
      payments: StaticUtility.parsePaymentsMap(json['payments']),
    );
  }
}
