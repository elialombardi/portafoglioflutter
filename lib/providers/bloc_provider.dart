import 'package:flutter/material.dart';
import 'package:portafoglioflutter/api/api.dart';
import 'package:portafoglioflutter/blocs/accounts_bloc.dart';
import 'package:portafoglioflutter/blocs/auth_bloc.dart';
import 'package:portafoglioflutter/blocs/payments_bloc.dart';
import 'package:portafoglioflutter/blocs/users_bloc.dart';

class BlocProvider extends InheritedWidget {
  final Api api;
  final String appName;
  final String flavorName;
  final String apiBaseUrl;
  final AuthBloc authBloc;
  final UsersBloc usersBloc;
  final AccountsBloc accountsBloc;
  final PaymentsBloc paymentsBloc;

  BlocProvider(
      {
        Key key,
        Widget child,      
        @required this.appName,
        @required this.flavorName,
        @required this.apiBaseUrl,
        @required this.api,
        AuthBloc authBloc,
        UsersBloc usersBloc,
        AccountsBloc accountsBloc,
        PaymentsBloc paymentsBloc
      })
      : this.authBloc = authBloc ?? AuthBloc(api),
      this.usersBloc = usersBloc ?? UsersBloc(api),
      this.accountsBloc = accountsBloc ?? AccountsBloc(api),
      this.paymentsBloc = paymentsBloc ?? PaymentsBloc(api),
      super(child: child, key: key) {
      print('new bloc provider');
    // authenticationBloc.activeUser.pipe(thingBloc.setActiveUser);
  }

  static BlocProvider of(BuildContext context) =>
      (context.inheritFromWidgetOfExactType(BlocProvider) as BlocProvider);

  @override
  bool updateShouldNotify(InheritedWidget oldWidget) => true;
}